//
//  InquiryViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/05/02.
//

import UIKit

class InquiryViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var backBtn: PressedView!
    @IBOutlet weak var trainerProfileImg: UIImageView!
    @IBOutlet weak var favoriteBtn: PressedView!
    @IBOutlet weak var favoriteImage: UIImageView!
    @IBOutlet weak var inquiryTableView: UITableView!
    
    @IBOutlet weak var messageText: UITextView!
    let inquiryCellNib = UINib(nibName: "InquiryTableViewCell", bundle: nil)
    @IBOutlet weak var moreBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inquiryTableView.register(inquiryCellNib, forCellReuseIdentifier: "inquiryCell")
        inquiryTableView.delegate = self
        inquiryTableView.dataSource = self
        inquiryTableView.separatorStyle = .none
        trainerProfileImg.layer.cornerRadius = 10
        placeholderSetting()
        let backGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleClose(sender:)))
        backBtn.addGestureRecognizer(backGesture)
        moreBtn.layer.cornerRadius = 13
    }
    
    func placeholderSetting() {
        messageText.delegate = self // txtvReview가 유저가 선언한 outlet
        messageText.text = "문의사항을 입력해주세요. \n최대 200자까지 가능합니다."
        messageText.textColor = UIColor(named: "INQUIRY_PLACE_HOLDER")
    }
    
    // TextView Place Holder
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor(named: "INQUIRY_PLACE_HOLDER") {
            textView.text = nil
            textView.textColor = UIColor(named: "GRAY_COLOR")
        }
        
    }
    // TextView Place Holder
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "문의사항을 입력해주세요. \n최대 200자까지 가능합니다."
            textView.textColor = UIColor(named: "INQUIRY_PLACE_HOLDER")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @objc func handleClose(sender: ValueTapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension InquiryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "inquiryCell") as? InquiryTableViewCell else {
            fatalError("InquiryTableViewCell이 존재 하지 않습니다.")
        }
        cell.selectionStyle = .none
        return cell
    }
    
    
}
