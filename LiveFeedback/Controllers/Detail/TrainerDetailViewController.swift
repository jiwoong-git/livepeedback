//
//  TrainerDetailViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/05/02.
//

import UIKit

class TrainerDetailViewController: UIViewController {

    @IBOutlet weak var backBtn: PressedView!
    @IBOutlet weak var detailDescript: UITextView!
    @IBOutlet weak var bottomButtonWrapper: UIView!
    @IBOutlet weak var inquiryBtn: PressedView!
    @IBOutlet weak var chatBtn: PressedView!
    @IBOutlet weak var detailTagOne: UIView!
    @IBOutlet weak var detailTagTwo: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitView()
        setDetailDescript()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func setInitView() {
        let backGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleBack(sender:)))
        backBtn.addGestureRecognizer(backGesture)
        
        detailTagOne.layer.cornerRadius = 8
        detailTagTwo.layer.cornerRadius = 8
        
        bottomButtonWrapper.layer.cornerRadius = 10
        bottomButtonWrapper.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        inquiryBtn.layer.cornerRadius = 10
        inquiryBtn.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        chatBtn.layer.cornerRadius = 10
        chatBtn.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        let inquiryGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleInquiry(sender:)))
        inquiryBtn.addGestureRecognizer(inquiryGesture)
        
        let chatGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleChat(sender:)))
        chatBtn.addGestureRecognizer(chatGesture)
    }

    func setDetailDescript() {
        let attributedString = NSMutableAttributedString(string: "경력\r현) 별별피트니스센터 트레이너\r트레이너 경력 3년차\r** 대회 입상\r경력사항/ 수상경력/ 학력 등 \r\r간단 소개\r안녕하세요. 슈라 트레이너 입니다. 운동을 처음 하시는 분도\r어렵지 않도록 블라 블라~ 다이어트 블라 블라 \r간단한 소개를 씁니다.\r\r프로그램 소개\r<기초체력>\r 집에 있는 간단한 물건이나, 맨손 운동으로 기초체력을 키울\r수 있게 도와드립니다.\r<다이어트>\r식단관리와 지방을 태우는 유산소 트레이닝을 도와드립니다.\r\r근무헬스장\r안양시 동안구, 별별피트니스센터 트레이너", attributes: [
            .font: UIFont.systemFont(ofSize: 11),
          .foregroundColor: UIColor(named: "GRAY_COLOR")
        ])
        attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 13, weight: .black), range: NSRange(location: 0, length: 2))
        attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 13, weight: .black), range: NSRange(location: 60, length: 5))
        attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 13, weight: .black), range: NSRange(location: 141, length: 7))
        attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 13, weight: .black), range: NSRange(location: 243, length: 5))
        detailDescript.attributedText = attributedString
    }

    @objc func handleBack(sender:ValueTapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func handleInquiry(sender:ValueTapGestureRecognizer) {
        let inquiryVc = InquiryViewController(nibName: "InquiryViewController", bundle: nil)
        inquiryVc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(inquiryVc, animated: true)
    }
    
    @objc func handleChat(sender:ValueTapGestureRecognizer) {
        // 여기서 결제 여부 판단하여 결제창 및 채팅리스트로 분기 처리
        self.tabBarController?.selectedIndex = 1
    }
}
