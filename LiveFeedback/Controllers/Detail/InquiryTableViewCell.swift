//
//  InquiryTableViewCell.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/05/03.
//

import UIKit

class InquiryTableViewCell: UITableViewCell {

    @IBOutlet weak var answerCheck: PressedView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        answerCheck.layer.cornerRadius = 5
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
