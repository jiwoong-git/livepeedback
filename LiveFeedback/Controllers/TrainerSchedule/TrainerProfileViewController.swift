//
//  TrainerScheduleViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/06/22.
//

import UIKit

class TrainerProfileViewController: UIViewController {

    @IBOutlet weak var trainerProfileImage: UIImageView!
    @IBOutlet weak var backBtn: PressedView!
    @IBOutlet weak var trainerNameLabel: UILabel!
    @IBOutlet weak var feedbackReceiveBtn: PressedView!
    @IBOutlet weak var feedbackCollectBtn: PressedView!
    @IBOutlet weak var scheduleBtn: PressedView!
    
    let scheduleSB:UIStoryboard = UIStoryboard(name: "Schedule", bundle: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let backGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleClose(sender:)))
        backBtn.addGestureRecognizer(backGesture)
        let combination = NSMutableAttributedString()
        combination.append(NSMutableAttributedString()
                            .attributedText(boldText: "슈라 ", normalText: "", fontSize: 16))
        combination.append(NSMutableAttributedString()
                            .attributedText(boldText: "", normalText: "트레이너", fontSize: 13))
        trainerNameLabel.attributedText = combination
        let feedbackReceiveGesture = ValueTapGestureRecognizer(target: self, action: #selector(handlefeedbackReceive(sender:)))
        feedbackReceiveBtn.addGestureRecognizer(feedbackReceiveGesture)
        
        let feedbackCollectGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleFeedbackCollect(sender:)))
        feedbackCollectBtn.addGestureRecognizer(feedbackCollectGesture)
        
        let scheduleGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleSchedule(sender:)))
        scheduleBtn.addGestureRecognizer(scheduleGesture)
    }
    
    @objc func handleClose(sender: ValueTapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func handlefeedbackReceive(sender: ValueTapGestureRecognizer) {
        if let chattingDetailVc = ChattingDetailViewController(nibName: "ChattingDetailViewController", bundle: nil) as? ChattingDetailViewController {
            if let navigator = navigationController {
                chattingDetailVc.hidesBottomBarWhenPushed = true
                navigator.navigationBar.isHidden = true
                navigator.pushViewController(chattingDetailVc, animated: true)
            }
        }
    }
    
    @objc func handleFeedbackCollect(sender: ValueTapGestureRecognizer) {
        if let feedbackAllVc = FeedbackAllViewController(nibName: "FeedbackAllViewController", bundle: nil) as? FeedbackAllViewController {
            if let navigator = navigationController {
                feedbackAllVc.hidesBottomBarWhenPushed = true
                navigator.navigationBar.isHidden = true
                navigator.pushViewController(feedbackAllVc, animated: true)
            }
        }
    }
    
    @objc func handleSchedule(sender: ValueTapGestureRecognizer) {
        let scheduleVc = scheduleSB.instantiateViewController(withIdentifier: "ScheduleCenterVc")
        navigationController?.pushViewController(scheduleVc, animated: true)
    }
}
