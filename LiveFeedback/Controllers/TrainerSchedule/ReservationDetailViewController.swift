//
//  ReservationDetailViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/06/27.
//

import UIKit

class ReservationDetailViewController: UIViewController {
    
    @IBOutlet weak var backBtn: PressedView!
    @IBOutlet weak var ptTrainerInfo: UIView!
    @IBOutlet weak var trainerProfileImg: UIImageView!
    
    @IBOutlet weak var ptTrainerInfoTitle: UIView!
    @IBOutlet weak var ptStudyDetail: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setInitView()
    }
    
    private func setInitView() {
        let backGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleClose(sender:)))
        backBtn.addGestureRecognizer(backGesture)
        ptTrainerInfo.layer.cornerRadius = 10
        ptTrainerInfoTitle.layer.cornerRadius = 10
        ptTrainerInfoTitle.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        ptTrainerInfo.layer.borderWidth = 1
        ptTrainerInfo.layer.borderColor = UIColor(named: "BRIGHT_ORANGE_TWO")?.cgColor
        ptStudyDetail.layer.cornerRadius = 10
        ptStudyDetail.layer.borderWidth = 1
        ptStudyDetail.layer.borderColor = UIColor(named: "BRIGHT_ORANGE_TWO")?.cgColor
        trainerProfileImg.layer.cornerRadius = 10
    }
    
    @objc func handleClose(sender: ValueTapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
}
