//
//  ReservationViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/06/27.
//

import UIKit
import SwiftEventBus

class ReservationViewController: UIViewController {

    
    @IBOutlet weak var backBtn: PressedView!
    @IBOutlet weak var reservationBtn: CustomUIButton!
    @IBOutlet weak var scheduleListBtn: CustomUIButton!
    @IBOutlet weak var scheduleInfoBtn: UIButton!
    @IBOutlet weak var timeDayView: UIView!
    @IBOutlet weak var timeDayImageView: UIImageView!
    @IBOutlet weak var monView: UIView!
    @IBOutlet weak var tueView: UIView!
    @IBOutlet weak var wedView: UIView!
    @IBOutlet weak var thuView: UIView!
    @IBOutlet weak var friView: UIView!
    @IBOutlet weak var timeTableView: UITableView!
    
    let currentIndex: Int = 0
    
    var times = 0...25
    let firstTime: Int = 9
    var tempIndex: Int = 0
    
    let timesCellNib = UINib(nibName: "TimesTableViewCell", bundle: nil)
    
    var commonAlertModalVc: CommonAlertModalViewController?
    var commonConfirmModalVc: CommonConfirmModalViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleBack(sender:)))
        backBtn.addGestureRecognizer(backGesture)
        backBtn.layer.cornerRadius = 8
        
        reservationBtn.layer.cornerRadius = 10
        reservationBtn.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        scheduleListBtn.layer.cornerRadius = 10
        scheduleListBtn.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        timeDayView.layer.borderColor = UIColor(named: "SCHEDULE_BORDER")?.cgColor
        timeDayView.layer.borderWidth = 1
        
        monView.layer.borderColor = UIColor(named: "SCHEDULE_BORDER")?.cgColor
        monView.layer.borderWidth = 1
        
        tueView.layer.borderColor = UIColor(named: "SCHEDULE_BORDER")?.cgColor
        tueView.layer.borderWidth = 1
        
        wedView.layer.borderColor = UIColor(named: "SCHEDULE_BORDER")?.cgColor
        wedView.layer.borderWidth = 1
        
        thuView.layer.borderColor = UIColor(named: "SCHEDULE_BORDER")?.cgColor
        thuView.layer.borderWidth = 1
        
        friView.layer.borderColor = UIColor(named: "SCHEDULE_BORDER")?.cgColor
        friView.layer.borderWidth = 1
        //콘텍스트를 이미지뷰 크기와 같게 설정

        UIGraphicsBeginImageContext(timeDayImageView.frame.size)
        //생성한 콘텍스트의 정보를 가져온다.

        let context = UIGraphicsGetCurrentContext()!
        //선굵기 설정
        context.setLineWidth(1.0)
        //선 칼라 설정
        context.setStrokeColor(UIColor(named: "SCHEDULE_BORDER")!.cgColor)
        //시작위치로 커서 이동
        context.move(to: CGPoint(x:0, y:0))
        context.addLine(to: CGPoint(x:timeDayImageView.bounds.maxX, y:timeDayImageView.bounds.maxY))
        context.strokePath()
        timeDayImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        timeTableView.register(timesCellNib, forCellReuseIdentifier: "timesCell")
        timeTableView.delegate = self
        timeTableView.dataSource = self
        timeTableView.separatorStyle = .none
    }
    
    @objc func handleBack(sender: ValueTapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func handleReservation(_ sender: CustomUIButton) {
        SwiftEventBus.post("changeCurrentPage", sender: ["currentIndex": 0])
    }
    
    @IBAction func handleScheduleList(_ sender: CustomUIButton) {
        SwiftEventBus.post("changeCurrentPage", sender: ["currentIndex": 1])
    }
    
    @IBAction func handleScheduleInfo(_ sender: Any) {
        if let scheduleInfoVc = ScheduleInfoViewController(nibName: "ScheduleInfoViewController", bundle: nil) as? ScheduleInfoViewController {
            if let navigator = navigationController {
                scheduleInfoVc.hidesBottomBarWhenPushed = true
                navigator.navigationBar.isHidden = true
                navigator.pushViewController(scheduleInfoVc, animated: true)
            }
        }
    }
    
    @objc func handleConfirmOne() {
        commonConfirmModalVc = CommonConfirmModalViewController(nibName: "CommonConfirmModalViewController", bundle: nil) as? CommonConfirmModalViewController
        if commonConfirmModalVc != nil {
            commonConfirmModalVc!.definesPresentationContext = true
            commonConfirmModalVc!.modalPresentationStyle = .overCurrentContext
            commonConfirmModalVc!.message = "2021/4/5 월요일 18:00~19:00\n등록을 취소하기겠습니까?\n취소시 현재 순번으로 다시 등록되지 않을 수 있습니다.\n현재 대기 순번 : 1"
            commonConfirmModalVc!.confirmText = "대기 취소"
            self.present(commonConfirmModalVc!, animated: true) {
                self.commonConfirmModalVc!.confirmBtn.addTarget(self, action: #selector(self.handleConfirmOK), for: .touchUpInside)
            }
        }
    }
    
    @objc func handleConfirmTwo() {
        commonConfirmModalVc = CommonConfirmModalViewController(nibName: "CommonConfirmModalViewController", bundle: nil) as? CommonConfirmModalViewController
        if commonConfirmModalVc != nil {
            commonConfirmModalVc!.definesPresentationContext = true
            commonConfirmModalVc!.modalPresentationStyle = .overCurrentContext
            commonConfirmModalVc!.message = "2021/4/7 수요일 18:00 ~ 19:00\n예약 대기 등록을 하시겠습니까?\n이전 등록자 취소시 자동으로 예약이 확정됩니다.\n대기인원 : 2명"
            commonConfirmModalVc!.confirmText = "예약 대기"
            self.present(commonConfirmModalVc!, animated: true) {
                self.commonConfirmModalVc!.confirmBtn.addTarget(self, action: #selector(self.handleConfirmOK), for: .touchUpInside)
            }
        }
    }
    
    @objc func handleConfirmThree() {
        commonConfirmModalVc = CommonConfirmModalViewController(nibName: "CommonConfirmModalViewController", bundle: nil) as? CommonConfirmModalViewController
        if commonConfirmModalVc != nil {
            commonConfirmModalVc!.definesPresentationContext = true
            commonConfirmModalVc!.modalPresentationStyle = .overCurrentContext
            commonConfirmModalVc!.message = "2021/4/5 월요일\n18:00 ~ 19:00\n등록하시겠습니까?"
            commonConfirmModalVc!.confirmText = "등록"
            self.present(commonConfirmModalVc!, animated: true) {
                self.commonConfirmModalVc!.confirmBtn.addTarget(self, action: #selector(self.handleConfirmOK), for: .touchUpInside)
            }
        }
    }
    
    @objc func handleConfirmOK() {
        alert("OK처리")
    }
}

extension ReservationViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return times.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "timesCell") as? TimesTableViewCell else {
            fatalError("TimesTableViewCell 을 찾을 수 없습니다.")
        }
        // ** 삭제 예정
        if indexPath.row == 3 {
            cell.monBtn.setTitle("팝업테스트1", for: .normal)
            cell.monBtn.addTarget(self, action: #selector(handleConfirmOne), for: .touchUpInside)
        }
        if indexPath.row == 4 {
            cell.thuBtn.setTitle("팝업테스트2", for: .normal)
            cell.thuBtn.addTarget(self, action: #selector(handleConfirmTwo), for: .touchUpInside)
        }
        if indexPath.row == 5 {
            cell.friBtn.setTitle("팝업테스트3", for: .normal)
            cell.friBtn.addTarget(self, action: #selector(handleConfirmThree), for: .touchUpInside)
        }
        // ** 삭제 예정 끝
        
        if indexPath.row % 2 == 0 {
            cell.timeLabel.isHidden = false
            cell.timeLine.isHidden = false
            let time = String(firstTime + tempIndex)
            cell.timeLabel.text = "\(time.count == 1 ? ("0")+time : time)"
            cell.monBottomLine.backgroundColor = UIColor(named: "SCHEDULE_BORDER")
            cell.tueBottomLine.backgroundColor = UIColor(named: "SCHEDULE_BORDER")
            cell.wedBottomLine.backgroundColor = UIColor(named: "SCHEDULE_BORDER")
            cell.thuBottomLine.backgroundColor = UIColor(named: "SCHEDULE_BORDER")
            cell.friBottomLine.backgroundColor = UIColor(named: "SCHEDULE_BORDER")
            
            tempIndex += 1
        } else {
            cell.monBottomLine.createDottedLine(width: 1, color: UIColor(named: "SCHEDULE_BORDER")!.cgColor)
            cell.tueBottomLine.createDottedLine(width: 1, color: UIColor(named: "SCHEDULE_BORDER")!.cgColor)
            cell.wedBottomLine.createDottedLine(width: 1, color: UIColor(named: "SCHEDULE_BORDER")!.cgColor)
            cell.thuBottomLine.createDottedLine(width: 1, color: UIColor(named: "SCHEDULE_BORDER")!.cgColor)
            cell.friBottomLine.createDottedLine(width: 1, color: UIColor(named: "SCHEDULE_BORDER")!.cgColor)
            cell.timeLabel.isHidden = true
            cell.timeLine.isHidden = true
        }
        
        return cell
    }
}
