//
//  DaysTableViewCell.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/06/28.
//

import UIKit

class TimesTableViewCell: UITableViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var timeLine: UIView!
    @IBOutlet weak var timeLabelView: UIView!
    
    @IBOutlet weak var monBtn: UIButton!
    @IBOutlet weak var monView: UIView!
    @IBOutlet weak var monBottomLine: UIView!
    
    @IBOutlet weak var tueBtn: UIButton!
    @IBOutlet weak var tueView: UIView!
    @IBOutlet weak var tueBottomLine: UIView!
    
    @IBOutlet weak var wedBtn: UIButton!
    @IBOutlet weak var wedView: UIView!
    @IBOutlet weak var wedBottomLine: UIView!
    
    @IBOutlet weak var thuBtn: UIButton!
    @IBOutlet weak var thuView: UIView!
    @IBOutlet weak var thuBottomLine: UIView!
    
    @IBOutlet weak var friBtn: UIButton!
    @IBOutlet weak var friView: UIView!
    @IBOutlet weak var friBottomLine: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        timeLabel.isHidden = true
        timeLine.isHidden = true
        
        timeLabelView.layer.borderWidth = 1
        timeLabelView.layer.borderColor = UIColor(named: "SCHEDULE_BORDER")?.cgColor
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
