//
//  ScheduleListViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/06/27.
//

import UIKit
import SwiftEventBus

class ScheduleListViewController: UIViewController {

    let currentIndex: Int = 1
    
    @IBOutlet weak var backBtn: PressedView!
    @IBOutlet weak var scheduleListBtn: CustomUIButton!
    @IBOutlet weak var reservationBtn: CustomUIButton!
    
    @IBOutlet weak var scheduleListTableView: UITableView!
    
    let scheduleListCellNib = UINib(nibName: "ScheduleListTableViewCell", bundle: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let backGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleBack(sender:)))
        backBtn.addGestureRecognizer(backGesture)
        backBtn.layer.cornerRadius = 8
        
        reservationBtn.layer.cornerRadius = 10
        reservationBtn.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        scheduleListBtn.layer.cornerRadius = 10
        scheduleListBtn.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        scheduleListTableView.delegate = self
        scheduleListTableView.dataSource = self
        scheduleListTableView.register(scheduleListCellNib, forCellReuseIdentifier: "scheduleListCell")
        scheduleListTableView.separatorStyle = .none
    }
    
    @IBAction func handleReservation(_ sender: CustomUIButton) {
        SwiftEventBus.post("changeCurrentPage", sender: ["currentIndex": 0])
    }
    
    @IBAction func handleScheduleList(_ sender: CustomUIButton) {
        SwiftEventBus.post("changeCurrentPage", sender: ["currentIndex": 1])
    }
    
    @objc func handleBack(sender: ValueTapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension ScheduleListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "scheduleListCell") as? ScheduleListTableViewCell else {
            fatalError("ScheduleListTableViewCell 을 찾을 수 없습니다.")
        }
       
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let reservationDetailVc = ReservationDetailViewController(nibName: "ReservationDetailViewController", bundle: nil) as? ReservationDetailViewController {
            if let navigator = navigationController {
                reservationDetailVc.hidesBottomBarWhenPushed = true
                navigator.navigationBar.isHidden = true
                navigator.pushViewController(reservationDetailVc, animated: true)
            }
        }
    }
}
