//
//  FeedbackAllViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/06/26.
//

import UIKit

class FeedbackAllViewController: UIViewController {
    
    @IBOutlet weak var backBtn: PressedView!
    @IBOutlet weak var uploadCollectionView: UICollectionView!
    
    let driveImageCellNib = UINib(nibName: "DriveImageCollectionViewCell", bundle: nil)
    let driveVideoCellNib = UINib(nibName: "DriveVideoCollectionViewCell", bundle: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let backGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleClose(sender:)))
        backBtn.addGestureRecognizer(backGesture)
        uploadCollectionView.delegate = self
        uploadCollectionView.dataSource = self
        uploadCollectionView.register(driveImageCellNib, forCellWithReuseIdentifier: "driveImageCell")
        uploadCollectionView.register(driveVideoCellNib, forCellWithReuseIdentifier: "driveVideoCell")
    }
    
    @objc func handleClose(sender: ValueTapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension FeedbackAllViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let driveImageCell = collectionView.dequeueReusableCell(withReuseIdentifier: "driveImageCell", for: indexPath) as? DriveImageCollectionViewCell else {
            return UICollectionViewCell()
        }
        guard let driveVideoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "driveVideoCell", for: indexPath) as? DriveVideoCollectionViewCell else {
            return UICollectionViewCell()
        }
        if true {
            return driveVideoCell
        } else {
            return driveImageCell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if let driveDetailVc = DriveDetailViewController(nibName: "DriveDetailViewController", bundle: nil) as? DriveDetailViewController {
            if let navigator = navigationController {
                driveDetailVc.hidesBottomBarWhenPushed = true
                navigator.navigationBar.isHidden = true
                navigator.pushViewController(driveDetailVc, animated: true)
            }
        }
    }
}
