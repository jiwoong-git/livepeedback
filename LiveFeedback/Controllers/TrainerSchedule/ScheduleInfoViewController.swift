//
//  ScheduleInfoViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/06/27.
//

import UIKit

class ScheduleInfoViewController: UIViewController {
    
    @IBOutlet weak var backBtn: PressedView!
    
    @IBOutlet weak var reservationOk: UIView!
    @IBOutlet weak var reservationComplete: UIView!
    @IBOutlet weak var reservationStandBy: UIView!
    @IBOutlet weak var myReservation: UIView!
    @IBOutlet weak var reservationImpossible: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleClose(sender:)))
        backBtn.addGestureRecognizer(backGesture)
        
        reservationOk.layer.borderWidth = 1
        reservationOk.layer.borderColor = UIColor(named: "BRIGHT_ORANGE_TWO")?.cgColor
        
        reservationComplete.layer.borderWidth = 1
        reservationComplete.layer.borderColor = UIColor(named: "BRIGHT_ORANGE_TWO")?.cgColor
        
        reservationStandBy.layer.borderWidth = 1
        reservationStandBy.layer.borderColor = UIColor(named: "BRIGHT_ORANGE_TWO")?.cgColor
        
        myReservation.layer.borderWidth = 1
        myReservation.layer.borderColor = UIColor(named: "BRIGHT_ORANGE_TWO")?.cgColor
        
        reservationImpossible.layer.borderWidth = 1
        reservationImpossible.layer.borderColor = UIColor(named: "BRIGHT_ORANGE_TWO")?.cgColor
    }
        
    @objc func handleClose(sender: ValueTapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }

}
