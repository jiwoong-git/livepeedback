//
//  ScheduleTableViewCell.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/06/27.
//

import UIKit

class ScheduleListTableViewCell: UITableViewCell {

    @IBOutlet weak var listViewWrapper: UIView! {
        didSet {
            listViewWrapper.layer.cornerRadius = 10
            listViewWrapper.backgroundColor = UIColor.clear
            listViewWrapper.layer.shadowOpacity = 0.3
            listViewWrapper.layer.shadowRadius = 2
            listViewWrapper.layer.shadowColor = UIColor.black.cgColor
            listViewWrapper.layer.shadowOffset = CGSize(width: 3, height: 3)
            listViewWrapper.layer.shadowRadius = 3
        }
    }
    @IBOutlet weak var shadowLayer: UIView! {
        didSet {
            shadowLayer.layer.cornerRadius = 10
            shadowLayer.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var reservationType: UIView!
    @IBOutlet weak var reservationAt: UILabel!
    @IBOutlet weak var detailBtn: PressedView!
    
    @IBOutlet weak var trainerProfileImg: UIImageView!
    @IBOutlet weak var trainerName: UILabel!
    @IBOutlet weak var reservationTime: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var ptEndCount: UILabel!
    @IBOutlet weak var penaltyCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        shadowLayer.layer.cornerRadius = 10
        shadowLayer.layer.masksToBounds = true
        
        trainerProfileImg.layer.cornerRadius = 10
        bottomView.layer.cornerRadius = 10
        bottomView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        bottomView.backgroundColor = UIColor(named: "BRIGHT_ORANGE_TWO")?.withAlphaComponent(0.5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
