//
//  ScheduleCenterViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/06/22.
//

import UIKit
import SwiftEventBus

class ScheduleCenterViewController: UIPageViewController {

    lazy var VCArray:[UIViewController] = {
        return [self.VCInstance(name: "ReservationVc"),
                self.VCInstance(name: "ScheduleListVc")]
    }()
    
    private func VCInstance(name: String) -> UIViewController {
        return UIStoryboard(name: "Schedule", bundle: nil).instantiateViewController(withIdentifier: name)
    }
    
    var currentIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
        
        if let firstVC = VCArray.first{
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        }
        
        SwiftEventBus.onMainThread(self, name: "changeCurrentPage") { result in
            let object: Dictionary<String, Any> = result?.object as! Dictionary<String, Any>
            let index = object["currentIndex"] as! Int
            self.setViewcontrollersFromIndex(index: index)
        }
        
    }
    
    func setViewcontrollersFromIndex(index : Int){
          if index < 0 && index >= VCArray.count {return }
        if index == currentIndex {
            return
        } else {
            if index == 0 {
                self.setViewControllers([VCArray[index]], direction: .reverse, animated: true, completion: nil)
            } else {
                self.setViewControllers([VCArray[index]], direction: .forward, animated: true, completion: nil)
            }
            currentIndex = index
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for view in self.view.subviews {
            if view is UIScrollView{
                view.frame = UIScreen.main.bounds
            } else if view is UIPageControl{
                view.backgroundColor = UIColor.clear
            }
        }
    }
}

extension ScheduleCenterViewController: UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = VCArray.firstIndex(of: viewController) else {
            return nil
        }
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        guard VCArray.count > previousIndex else{
            return nil
        }
        currentIndex = previousIndex
        return VCArray[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = VCArray.firstIndex(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        
        guard nextIndex < VCArray.count else {
            return nil
        }
        
        guard VCArray.count > nextIndex else{
            return nil
        }
        currentIndex = nextIndex
        return VCArray[nextIndex]
    }
}
