//
//  CommonModalViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/04/28.
//

import UIKit
import SwiftEventBus

class LoginAlertModalViewController: UIViewController {

    @IBOutlet weak var okBtn: PressedView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(named: "BG_WHITE")?.withAlphaComponent(0.75)
        view.isOpaque = false
        let okGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleOk(sender:)))
        okBtn.addGestureRecognizer(okGesture)
        okBtn.layer.cornerRadius = 13
    }
    
    
    @objc func handleOk(sender: ValueTapGestureRecognizer) {
        self.dismiss(animated: true) {
            SwiftEventBus.post("goLoginPage")
        }
    }

}
