//
//  ViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/03/17.
//

import UIKit

class MainTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.barTintColor = .white
        self.tabBar.isTranslucent = false
        tabBar.shadowImage = UIImage()
        tabBar.backgroundImage = UIImage()
        // Do any additional setup after loading the view.
    }
}

