//
//  SearchViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/03/26.
//

import UIKit

class SearchViewController: UIViewController {
    
    @IBOutlet weak var searchInputView: UIView!
    @IBOutlet weak var searchInput: UITextField!
    @IBOutlet weak var searchResultTableView: UITableView!
    
    let searchDetailCellNib = UINib(nibName: "SearchResultTableViewCell", bundle: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchInputView.layer.cornerRadius = 10
        let clearBtn = UIButton()
        clearBtn.setImage(UIImage(named: "ic_close_cicle"), for: .normal)
        clearBtn.frame = CGRect(x: CGFloat(searchInput.frame.size.width - 16), y: 4, width: 16, height: 16)
        clearBtn.addTarget(self, action: #selector(self.searchClearText(_:)), for: .touchUpInside)
        searchInput.rightView = clearBtn
        searchInput.rightViewMode = .never
        searchResultTableView.delegate = self
        searchResultTableView.dataSource = self
        searchResultTableView.register(searchDetailCellNib, forCellReuseIdentifier: "searchDetailCell")
    }
    
    @IBAction func handleBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @objc func searchClearText(_ sender: UIButton) {
        searchInput.text = ""
        searchInput.rightViewMode = .never
    }
    
    @IBAction func handelSearchChange(_ sender: Any) {
        if searchInput.text!.count > 0 {
            searchInput.rightViewMode = .whileEditing
        } else {
            searchInput.rightViewMode = .never
        }
    }
}

extension SearchViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "searchDetailCell") as? SearchResultTableViewCell else {
            return UITableViewCell()
        }
        cell.backView.layer.cornerRadius = 10
        cell.backView.clipsToBounds = true
        cell.backView.layer.shadowColor = UIColor.black.cgColor
        cell.backView.layer.shadowOffset = CGSize(width: 3, height: 3)
        cell.backView.layer.shadowRadius = 10.0
        cell.backView.layer.shadowOpacity = 0.2
        cell.backView.layer.masksToBounds = false
        return cell
    }
}
