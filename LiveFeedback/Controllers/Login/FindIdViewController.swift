//
//  FindIdViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/05/01.
//

import UIKit

class FindIdViewController: UIViewController {

    @IBOutlet weak var closeBtn: PressedView!
    
    @IBOutlet weak var findIdBtn: PressedView!
    @IBOutlet weak var findPwBtn: PressedView!
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var userPhoneNumber: UITextField!
    @IBOutlet weak var submitSms: PressedView!
    
    @IBOutlet weak var findIdView: UIScrollView!
    
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var submitPassword: PressedView!
    @IBOutlet weak var findPwView: UIScrollView!
    
    var active: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        findIdView.isHidden = true
        findPwView.isHidden = true
        setInitView()
    }
    
    func setInitView() {
        let closeGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleClose(sender:)))
        closeBtn.addGestureRecognizer(closeGesture)
        findIdBtn.layer.cornerRadius = 10
        findIdBtn.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        findPwBtn.layer.cornerRadius = 10
        findPwBtn.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        submitSms.layer.cornerRadius = 22
        let submitSmsGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleSubmitSms(sender:)))
        submitSms.addGestureRecognizer(submitSmsGesture)
        submitPassword.layer.cornerRadius = 22
        let submitPasswordGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleSubmitPassword(sender:)))
        submitPassword.addGestureRecognizer(submitPasswordGesture)
        setViewEnable()
        userName.attributedPlaceholder = NSAttributedString(string: "이름을 입력해주세요.",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "BG_GRAY") as Any])
        userPhoneNumber.attributedPlaceholder = NSAttributedString(string: "가입하신 휴대폰번호를 입력해주세요.",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "BG_GRAY") as Any])
        
        userEmail.attributedPlaceholder = NSAttributedString(string: "가입하신 이메일 주소를 입력해주세요.",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "BG_GRAY") as Any])
        let findIdGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleActiveChange(sender:)))
        findIdGesture.customTag = 0
        findIdBtn.addGestureRecognizer(findIdGesture)
        let findPwGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleActiveChange(sender:)))
        findPwGesture.customTag = 1
        findPwBtn.addGestureRecognizer(findPwGesture)
    }
    
    @objc func handleClose(sender: ValueTapGestureRecognizer) {
        self.dismiss(animated: true)
    }
    
    @objc func handleActiveChange(sender: ValueTapGestureRecognizer) {
        let position = (sender.customTag as? Int ?? -1)
        active = position
        setViewEnable()
    }
    
    func setViewEnable() {
        if active == 0 {
            findIdView.isHidden = false
            findPwView.isHidden = true
            findIdBtn.setBackgroundColor(color: UIColor(named: "BRIGHT_ORANGE_TWO")!)
            findIdBtn.setTintColor(color: UIColor(named: "LOGIN_INPUT_BG")!)
            
            findPwBtn.setBackgroundColor(color: UIColor(named: "MAIN_BUTTON_LINE")!)
            findPwBtn.setTintColor(color: UIColor(named: "LOGIN_INPUT_BG")!)
            
        } else {
            findIdView.isHidden = true
            findPwView.isHidden = false
            findPwBtn.setBackgroundColor(color: UIColor(named: "BRIGHT_ORANGE_TWO")!)
            findPwBtn.setTintColor(color: UIColor(named: "LOGIN_INPUT_BG")!)
            
            findIdBtn.setBackgroundColor(color: UIColor(named: "MAIN_BUTTON_LINE")!)
            findIdBtn.setTintColor(color: UIColor(named: "LOGIN_INPUT_BG")!)
        }
    }
    
    @objc func handleSubmitSms(sender: ValueTapGestureRecognizer) {
        self.alert("입력한 회원정보가 존재 하지 않습니다.")
    }
    
    @objc func handleSubmitPassword(sender: ValueTapGestureRecognizer) {
        self.alert("이메일 주소를 다시 확인해주세요.")
    }
}
