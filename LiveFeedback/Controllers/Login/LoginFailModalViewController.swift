//
//  LoginFailModalViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/05/01.
//

import UIKit
import SwiftEventBus

class LoginFailModalViewController: UIViewController {
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var modalView: UIView!
    @IBOutlet weak var cancelBtn: PressedView!
    @IBOutlet weak var findIdBtn: PressedView!
    
    let gradientLayer: CAGradientLayer = CAGradientLayer()
    override func viewDidLoad() {
        super.viewDidLoad()
        gradientLayer.colors = [UIColor(named: "BG_RED")!.cgColor, UIColor(named: "BG_ORANGE")!.cgColor]
        gradientLayer.shouldRasterize = true
        backgroundView.layer.addSublayer(gradientLayer)
        
        modalView.layer.cornerRadius = 10
        cancelBtn.layer.cornerRadius = 13
        findIdBtn.layer.cornerRadius = 13
        
        let cancelGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleCancel(sender:)))
        cancelBtn.addGestureRecognizer(cancelGesture)
        
        let findIdGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleFindId(sender:)))
        findIdBtn.addGestureRecognizer(findIdGesture)
    }
    override func viewDidLayoutSubviews() {
        gradientLayer.frame = self.backgroundView.bounds
    }
    
    @objc func handleCancel(sender: ValueTapGestureRecognizer) {
        self.dismiss(animated: true) {
            
        }
    }
    
    @objc func handleFindId(sender: ValueTapGestureRecognizer) {
        self.dismiss(animated: true) {
            SwiftEventBus.post("handleGoFindId")
        }
    }
}
