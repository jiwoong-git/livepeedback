//
//  LoginViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/04/28.
//

import UIKit
import SwiftEventBus

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var closeBtn: PressedView!
    @IBOutlet weak var emailInputView: UIView!
    @IBOutlet weak var emailInput: UITextField!
    @IBOutlet weak var passwordInputView: UIView!
    @IBOutlet weak var passwordInput: UITextField!
    @IBOutlet weak var loginBtn: PressedView!
    @IBOutlet weak var signupBtn: PressedView!
    
    @IBOutlet weak var kakaoBtn: PressedView!
    @IBOutlet weak var googleBtn: PressedView!
    @IBOutlet weak var appleBtn: PressedView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    let gradientLayer: CAGradientLayer = CAGradientLayer()
    
    var activeTextField : UITextField? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.hidesBottomBarWhenPushed = true
        gradientLayer.colors = [UIColor(named: "BG_RED")!.cgColor, UIColor(named: "BG_ORANGE")!.cgColor]
        gradientLayer.shouldRasterize = true
        backgroundView.layer.addSublayer(gradientLayer)
        setInitView()
        emailInput.delegate = self
        passwordInput.delegate = self
        //observer등록
        NotificationCenter.default.addObserver(self, selector: #selector(textViewMoveUp), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(textViewMoveDown), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        SwiftEventBus.onMainThread(self, name: "handleGoFindId") { result in
            self.handleGoFindId()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {        
        self.navigationController?.navigationBar.isHidden = false
    }
    
    // when user select a textfield, this method will be called
    func textFieldDidBeginEditing(_ textField: UITextField) {
      // set the activeTextField to the selected textfield
        self.activeTextField = textField
        let text = textField.text
        
        if (text?.isEmpty == false) {
            self.activeTextField?.rightViewMode = .always
        }
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        let tag = textField.tag
        let text = textField.text
        
        switch tag {
        case 0:
            if (text?.isEmpty == true) {
                self.emailInput.rightViewMode = .never
            } else {
                self.emailInput.rightViewMode = .always
            }
        case 1:
            if (text?.isEmpty == true) {
                self.passwordInput.rightViewMode = .never
            } else {
                self.passwordInput.rightViewMode = .always
            }
        default:
            return
        }
    }
      
    // when user click 'done' or dismiss the keyboard
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.activeTextField?.rightViewMode = .never
        self.activeTextField = nil
    }
    
    @objc func textViewMoveUp(_ notification: NSNotification){
        guard let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else {
          // if keyboard size is not available for some reason, dont do anything
          return
        }

        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height , right: 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    @objc func textViewMoveDown(_ notification: NSNotification){
        let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
                
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.emailInput.resignFirstResponder()
        self.passwordInput.resignFirstResponder()
    }
    
    override func viewDidLayoutSubviews() {
        gradientLayer.frame = self.backgroundView.bounds
    }
    
    func setInitView() {
        emailInputView.layer.cornerRadius = 21
        passwordInputView.layer.cornerRadius = 21
        // 이메일 인풋 설정
        
        emailInput.tag = 0
        emailInput.attributedPlaceholder = NSAttributedString(string: "이메일을 입력하세요.",
                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "LOGIN_TEXT_COLOR") as Any])
        let emailClearBtn = UIButton()
        emailClearBtn.setImage(UIImage(named: "ic_login_input_x"), for: .normal)
        emailClearBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        emailClearBtn.frame = CGRect(x: CGFloat(emailInput.frame.size.width), y: CGFloat(5), width: CGFloat(24), height: CGFloat(24))
        emailClearBtn.addTarget(self, action: #selector(self.emailClearText(_:)), for: .touchUpInside)
        emailInput.rightView = emailClearBtn
        emailInput.addTarget(self, action: #selector(self.emailTextChange(_:)), for: .editingChanged)
        
        // 비밀번호 인풋 설정
        passwordInput.tag = 1
        passwordInput.attributedPlaceholder = NSAttributedString(string: "비밀번호를 입력하세요.",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "LOGIN_TEXT_COLOR") as Any])
        let passwordClearBtn = UIButton()
        passwordClearBtn.setImage(UIImage(named: "ic_login_input_x"), for: .normal)
        passwordClearBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        passwordClearBtn.frame = CGRect(x: CGFloat(passwordInput.frame.size.width ), y: CGFloat(5), width: CGFloat(24), height: CGFloat(24))
        passwordClearBtn.addTarget(self, action: #selector(self.passwordClearText(_:)), for: .touchUpInside)
        passwordInput.rightView = passwordClearBtn
        passwordInput.addTarget(self, action: #selector(self.passwordTextChange(_:)), for: .editingChanged)
        
        signupBtn.layer.cornerRadius = 21
        loginBtn.layer.cornerRadius = 21
        
        let signupGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleSignup(sender:)))
        signupBtn.addGestureRecognizer(signupGesture)
        
        let loginGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleLogin(sender:)))
        loginBtn.addGestureRecognizer(loginGesture)
        
        kakaoBtn.layer.cornerRadius = 10
        let kakaoGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleKakao(sender:)))
        kakaoBtn.addGestureRecognizer(kakaoGesture)
        googleBtn.layer.cornerRadius = 10
        let goolgeGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleGoogle(sender:)))
        googleBtn.addGestureRecognizer(goolgeGesture)
        appleBtn.layer.cornerRadius = 10
        let appleGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleApple(sender:)))
        appleBtn.addGestureRecognizer(appleGesture)
        let closeGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleClose(sender:)))
        closeBtn.addGestureRecognizer(closeGesture)
    }
    
    @objc func emailClearText(_ sender: UIButton) {
        emailInput.text = ""
        emailInput.rightViewMode = .never
    }
    
    @objc func emailTextChange(_ textField: UITextField) {
        if self.emailInput.text!.count > 0 {
            self.emailInput.rightViewMode = .always
        } else {
            self.emailInput.rightViewMode = .never
        }
    }
    
    @objc func passwordClearText(_ sender: UIButton) {
        passwordInput.text = ""
        passwordInput.rightViewMode = .never
    }
    
    @objc func passwordTextChange(_ textField: UITextField) {
        if self.passwordInput.text!.count > 0 {
            self.passwordInput.rightViewMode = .always
        } else {
            self.passwordInput.rightViewMode = .never
        }
    }
    
    @objc func handleSignup(sender: ValueTapGestureRecognizer) {
        let signUpVC = SignUpViewController(nibName: "SignUpViewController", bundle: nil)
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
    
    @objc func handleLogin(sender: ValueTapGestureRecognizer) {
        if false {
            self.navigationController?.popToRootViewController(animated: true)
        } else {
            let LoginFailModal = LoginFailModalViewController(nibName: "LoginFailModalViewController", bundle: nil)
            LoginFailModal.definesPresentationContext = true
            LoginFailModal.modalPresentationStyle = .overCurrentContext
            self.present(LoginFailModal, animated: true)
        }
    }
    
    @objc func handleKakao(sender: ValueTapGestureRecognizer) {
        self.alert("카카오 연동")
    }
    @objc func handleGoogle(sender: ValueTapGestureRecognizer) {
        self.alert("구글 연동")
    }
    @objc func handleApple(sender: ValueTapGestureRecognizer) {
        self.alert("애플 연동")
    }
    
    @objc func handleClose(sender: ValueTapGestureRecognizer) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func handleGoFindId() {
        let findIdModal = FindIdViewController(nibName: "FindIdViewController", bundle: nil)
        findIdModal.definesPresentationContext = true
        findIdModal.modalPresentationStyle = .overCurrentContext
        findIdModal.active = 0
        self.present(findIdModal, animated: true)
    }
}



