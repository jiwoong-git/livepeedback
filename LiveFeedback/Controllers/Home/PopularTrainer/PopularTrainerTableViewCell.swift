//
//  PopularTrainerTableViewCell.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/04/27.
//

import UIKit

class PopularTrainerTableViewCell: UITableViewCell {

    @IBOutlet weak var trainerImage: UIImageView!
    @IBOutlet weak var trainerName: UILabel!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var trainerDescript: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        trainerImage.layer.cornerRadius = 10
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
