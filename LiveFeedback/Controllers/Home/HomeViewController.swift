//
//  HomeViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/03/17.
//

import UIKit
import ImageSlideshow
import SwiftEventBus

/*
 mainType 으로 하단 데이터 처리하시면 됩니다.
 popular == 인기순
 recommendation == "추천순"
 new == "신규"
 bookmark == "즐겨찾기"
**/

class HomeViewController: UIViewController {
    
    @IBOutlet weak var sliderImage: ImageSlideshow!
    @IBOutlet weak var sliderLabel: UILabel!
    @IBOutlet weak var sliderBtn: PressedView!
    @IBOutlet weak var sliderBtnRightImg: UIImageView!
    
    @IBOutlet weak var topCollectionView: UICollectionView!
    @IBOutlet weak var popularTrainerTableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var bookmarkBtn: PressedView!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    
    let trainerCellNib = UINib(nibName: "PopularTrainerTableViewCell", bundle: nil)
    
    let pList = UserDefaults.standard
    
    var mainType = "popular"
    
    // 현재 로컬 이미지로 처리함 추후 이미지 Api 로 받아서 처리 할수 있도록..
    let localSource = [BundleImageSource(imageString: "slider_one"), BundleImageSource(imageString: "slider_two"), BundleImageSource(imageString: "slider_three")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        popularTrainerTableView.register(trainerCellNib, forCellReuseIdentifier: "trainerCell")
        popularTrainerTableView.separatorStyle = .none
        setInitSliderView()
        setNavigationBar()
        setBtnAction()
        
        SwiftEventBus.onMainThread(self, name: "goLoginPage") { result in
            self.goLoginPage()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        popularTrainerTableView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        popularTrainerTableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        popularTrainerTableView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize" {
            if object is UITableView {
                if let newValue = change?[.newKey] {
                    let newSize = newValue as! CGSize
                    self.tableViewHeight.constant = newSize.width
                }
            }
        }
    }
    
    func setInitSliderView() {
        sliderImage.slideshowInterval = 5.0
        sliderImage.contentScaleMode = UIViewContentMode.scaleAspectFill
        sliderImage.activityIndicator = DefaultActivityIndicator()
        sliderImage.delegate = self
        sliderLabel.layer.zPosition = 2
        sliderBtnRightImg.layer.zPosition = 2
        sliderBtn.layer.zPosition = 2
        sliderBtn.layer.cornerRadius = 20
        sliderBtnRightImg.image = sliderBtnRightImg.image?.withRenderingMode(.alwaysTemplate)
        sliderBtnRightImg.tintColor = UIColor(named:"BG_TOP")

        sliderImage.setImageInputs(localSource)
    }
    
    func setNavigationBar() {
        let navigationBar = self.navigationController?.navigationBar
        
        navigationBar!.barTintColor = UIColor(named: "GRAY000")
        navigationBar?.isTranslucent = false
        // NavigationBar Bottom Border Line Hidden 처리
        navigationBar!.shadowImage = UIImage()
        
        let imageView = UIImageView(image: UIImage(named: "logo"))
        imageView.contentMode = .scaleToFill
        navigationItem.titleView = imageView
    }
    
    func setBtnAction() {
        let bookmarkGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleTopBtn(sender:)))
        bookmarkGesture.customTag = 3
        bookmarkBtn.addGestureRecognizer(bookmarkGesture)
    }
    
    @IBAction func searchBtn(_ sender: Any) {
        let searchVC = SearchViewController(nibName: "SearchViewController", bundle: nil) as SearchViewController
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    @IBAction func notificationBtn(_ sender: Any) {
        let notiVC = NotificationViewController(nibName: "NotificationViewController", bundle: nil) as NotificationViewController
        self.navigationController?.pushViewController(notiVC, animated: true)
    }
    
    @objc func handleTopBtn(sender: ValueTapGestureRecognizer) {
        let position = (sender.customTag as? Int ?? -1)
        
        switch position {
            case 0:
                return
            case 1:
                return
            case 2:
                return
            case 3:
                let status = pList.bool(forKey: "logined")
                if status == true {
                    
                } else {
                    let loginAlertModal = LoginAlertModalViewController(nibName: "LoginAlertModalViewController", bundle: nil)
                    loginAlertModal.definesPresentationContext = true
                    loginAlertModal.modalPresentationStyle = .overCurrentContext
                    self.present(loginAlertModal, animated: false)
                }
                return
            default:
                print("case default : \(position)")
                return
        }
    }
    
    func goLoginPage() {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let loginController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        loginController.definesPresentationContext = true
        loginController.modalPresentationStyle = .overCurrentContext
        loginController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(loginController, animated: true)
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "trainerCell", for: indexPath) as? PopularTrainerTableViewCell else {
            fatalError("trainerCell이 없습니다.")
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let detailVc = TrainerDetailViewController(nibName: "TrainerDetailViewController", bundle: nil)
        detailVc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(detailVc, animated: true)
    }
    
}

extension HomeViewController: ImageSlideshowDelegate {
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "topCollectionViewCell", for: indexPath) as? TopCollectionViewCell else {
            fatalError("Unexpected indexPath")
        }
        cell.popularityImage.image = UIImage(named: "sample1")
        cell.popularityImage.layer.cornerRadius = 10
        return cell
    }
}
