//
//  popularityCollectionViewCell.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/04/27.
//

import UIKit

class TopCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var popularityImage: UIImageView!
    @IBOutlet weak var trainerName: UILabel!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var trainerDescription: UILabel!    
}
