//
//  TermsViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/05/01.
//

import UIKit

class TermsViewController: UIViewController {

    @IBOutlet weak var closeBtn: PressedView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let closeGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleCloes(sender:)))
        closeBtn.addGestureRecognizer(closeGesture)
    }
    
    @objc func handleCloes(sender: ValueTapGestureRecognizer) {
        self.dismiss(animated: true)
    }
    

}
