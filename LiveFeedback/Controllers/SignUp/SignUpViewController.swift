//
//  SignUpViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/04/29.
//

import UIKit

class SignUpViewController: UIViewController {

    @IBOutlet weak var backBtn: PressedView!
    @IBOutlet weak var userId: UITextField!
    @IBOutlet weak var userIdValidImg: UIImageView!
    @IBOutlet weak var userPw: UITextField!
    @IBOutlet weak var userPwValidImg: UIImageView!
    @IBOutlet weak var userPwConfirm: UITextField!
    @IBOutlet weak var userPwConfirmValidImg: UIImageView!
    @IBOutlet weak var policyAgreeBtn: PressedView!
    @IBOutlet weak var policyAgreeBtnImage: UIImageView!
    @IBOutlet weak var termsBtn: UIButton!
    @IBOutlet weak var privacyBtn: UIButton!
    @IBOutlet weak var nextCertifyBtn: PressedView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let backGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleBack(sender:)))
        backBtn.addGestureRecognizer(backGesture)
        backBtn.layer.cornerRadius = 4
        setInitView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @objc func handleBack(sender: ValueTapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    func setInitView() {
        self.userId?.attributedPlaceholder = NSAttributedString(string: "이메일을 입력해주세요.",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "BG_GRAY") as Any]
        )
        self.userPw?.attributedPlaceholder = NSAttributedString(string: "영문+숫자 조함 8자리 이상 입력해주세요.",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "BG_GRAY") as Any]
        )
        self.userPwConfirm?.attributedPlaceholder = NSAttributedString(string: "비밀번호를 한번 더 입력해주세요.",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "BG_GRAY") as Any]
        )
        
        nextCertifyBtn.layer.cornerRadius = 10
        nextCertifyBtn.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        policyAgreeBtn.layer.cornerRadius = policyAgreeBtn.bounds.width / 2
    }
        
    @IBAction func handleGoTerms(_ sender: UIButton) {
        let TermsModal = TermsViewController(nibName: "TermsViewController", bundle: nil)
        TermsModal.definesPresentationContext = true
        TermsModal.modalPresentationStyle = .overCurrentContext
        self.present(TermsModal, animated: true)
    }
    @IBAction func handleGoPrivacy(_ sender: Any) {
        
        let PrivacyModal = PrivacyViewController(nibName: "PrivacyViewController", bundle: nil)
        PrivacyModal.definesPresentationContext = true
        PrivacyModal.modalPresentationStyle = .overCurrentContext
        self.present(PrivacyModal, animated: true)
    }
}
