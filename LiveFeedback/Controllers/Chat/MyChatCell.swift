//
//  MyChatCell.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/06/21.
//

import UIKit

class MyChatCell: UITableViewCell {

    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var myChatContentView: UIView!
    @IBOutlet weak var myChatContent: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    
    let gradientLayer: CAGradientLayer = CAGradientLayer()
    override func awakeFromNib() {
        super.awakeFromNib()
        
        gradientLayer.colors = [UIColor(named: "BG_RED")!.cgColor, UIColor(named: "BG_ORANGE")!.cgColor]
        gradientLayer.shouldRasterize = true
        myChatContentView.layer.addSublayer(gradientLayer)
        // Initialization code
        myChatContentView.layer.cornerRadius = 8
        myChatContentView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
