//
//  ChattingDetailViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/06/21.
//

import UIKit

class ChattingDetailViewController: UIViewController {
    
    @IBOutlet weak var backBtn: PressedView!
    @IBOutlet weak var chatTableView: UITableView!
    @IBOutlet weak var trainerImage: UIImageView!
    
    let MyChatCellNib = UINib(nibName: "MyChatCell", bundle: nil)
    let TrainerChatCellNib = UINib(nibName: "TrainerChatCell", bundle: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let backGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleClose(sender:)))
        backBtn.addGestureRecognizer(backGesture)
        trainerImage.layer.cornerRadius = 8
        chatTableView.delegate = self
        chatTableView.dataSource = self
        chatTableView.register(MyChatCellNib, forCellReuseIdentifier: "MyChatCell")
        chatTableView.register(TrainerChatCellNib, forCellReuseIdentifier: "TrainerChatCell")
        chatTableView.separatorStyle = .none
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    @objc func handleClose(sender: ValueTapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func handleGoTrainerProfile(_ sender: Any) {
        if let trainerScheduleVc = TrainerProfileViewController(nibName: "TrainerProfileViewController", bundle: nil) as? TrainerProfileViewController {
            if let navigator = navigationController {
                trainerScheduleVc.hidesBottomBarWhenPushed = true
                navigator.navigationBar.isHidden = true
                navigator.pushViewController(trainerScheduleVc, animated: true)
            }
        }
         
    }
}

extension ChattingDetailViewController:UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let myCell = tableView.dequeueReusableCell(withIdentifier: "MyChatCell") as? MyChatCell else {
            fatalError("MyChatCell을 찾을 수 없습니다.")
        }
        guard let trainerCell = tableView.dequeueReusableCell(withIdentifier: "TrainerChatCell") as? TrainerChatCell else {
            fatalError("TrainerChatCellNib을 찾을 수 없습니다.")
        }
        if indexPath.row % 2 == 0 {
            myCell.selectionStyle = .none
            return myCell
        } else {
            trainerCell.selectionStyle = .none
            trainerCell.trainerImageButton.addTarget(self, action: #selector(handleGoTrainerProfile(_:)), for: .touchUpInside)
            return trainerCell
        }
        
    }
}
