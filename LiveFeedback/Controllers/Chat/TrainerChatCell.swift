//
//  TrainerChatCell.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/06/21.
//

import UIKit

class TrainerChatCell: UITableViewCell {

    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var trainerImageButton: UIButton!
    @IBOutlet weak var chatView: UIView!
    @IBOutlet weak var chatContent: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        chatView.layer.cornerRadius = 8
        chatView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        trainerImageButton.layer.cornerRadius = 8
        trainerImageButton.imageView?.layer.cornerRadius = 8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
