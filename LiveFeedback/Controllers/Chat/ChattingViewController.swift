//
//  ChattingViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/05/05.
//

import UIKit

class ChattingViewController: UIViewController {
    
    @IBOutlet weak var chattingTableView: UITableView!
    
    let chattingCellNib = UINib(nibName: "ChattingTableViewCell", bundle: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let attrs = [
            NSAttributedString.Key.foregroundColor: UIColor(named: "BRIGHT_ORANGE_TWO"),
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .black)
        ]
        navigationController?.navigationBar.titleTextAttributes = attrs as [NSAttributedString.Key : Any]
        chattingTableView.register(chattingCellNib, forCellReuseIdentifier: "chattingCell")
        chattingTableView.delegate = self
        chattingTableView.dataSource = self
        chattingTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
    }
    
    @IBAction func searchBtn(_ sender: Any) {
        let searchVC = SearchViewController(nibName: "SearchViewController", bundle: nil) as SearchViewController
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    @IBAction func notificationBtn(_ sender: Any) {
        let notiVC = NotificationViewController(nibName: "NotificationViewController", bundle: nil) as NotificationViewController
        self.navigationController?.pushViewController(notiVC, animated: true)
    }
}

extension ChattingViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "chattingCell", for: indexPath) as? ChattingTableViewCell else {
            fatalError("chattingCell을 찾을 수 없습니다.")
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let chattingDetailVc = ChattingDetailViewController(nibName: "ChattingDetailViewController", bundle: nil) as? ChattingDetailViewController {
            if let navigator = navigationController {
                chattingDetailVc.hidesBottomBarWhenPushed = true
                navigator.navigationBar.isHidden = true
                navigator.pushViewController(chattingDetailVc, animated: true)
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
