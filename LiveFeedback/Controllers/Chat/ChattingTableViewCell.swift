//
//  ChattingTableViewCell.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/05/12.
//

import UIKit

class ChattingTableViewCell: UITableViewCell {

    @IBOutlet weak var trainerProfileImg: UIImageView!
    @IBOutlet weak var tarinerName: UILabel!
    @IBOutlet weak var fixBtn: PressedView!
    @IBOutlet weak var fixImg: UIImageView!
    @IBOutlet weak var chattingText: UILabel!
    @IBOutlet weak var insertAt: UILabel!
    
    var fixed: Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let fixGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleFix(sender:)))
        // Initialization code
        fixBtn.addGestureRecognizer(fixGesture)
        trainerProfileImg.layer.cornerRadius = 8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @objc func handleFix(sender: ValueTapGestureRecognizer) {
        fixed = !fixed
        if fixed {
            fixImg.image = UIImage(named: "ic_pin_on")
        } else {
            fixImg.image = UIImage(named: "ic_pin")
        }
    }
    
}
