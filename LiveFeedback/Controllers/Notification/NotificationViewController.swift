//
//  NotificationViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/03/27.
//

import UIKit

class NotificationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var notiTableView: UITableView!
    @IBOutlet weak var backButton: PressedView!
    @IBOutlet weak var notiBtn: CustomUIButton!
    @IBOutlet weak var noticeBtn: CustomUIButton!
    
    let notiCellNib = UINib(nibName: "NotiTableViewCell", bundle: nil)
    let notiHeaderCellNib = UINib(nibName: "NotiHeaderTableViewCell", bundle: nil)
        
    override func viewDidLoad() {
        super.viewDidLoad()
        notiBtn.layer.cornerRadius = 10
        notiBtn.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        noticeBtn.layer.cornerRadius = 10
        noticeBtn.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
       
        notiTableView.register(notiCellNib, forCellReuseIdentifier: "notiCell")
        notiTableView.register(notiHeaderCellNib, forCellReuseIdentifier: "notiHeaderCell")
        
        let backGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleBack(sender:)))
        backButton.addGestureRecognizer(backGesture)
        backButton.layer.cornerRadius = 8
        notiTableView.delegate = self
        notiTableView.dataSource = self
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @objc func handleBack(sender: ValueTapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "notiCell") as? NotiTableViewCell else {
            return UITableViewCell()
        }
        guard let headerCell = tableView.dequeueReusableCell(withIdentifier: "notiHeaderCell") as? NotiHeaderTableViewCell else {
            return UITableViewCell()
        }
        if indexPath.row % 5 == 0 {
            headerCell.notiWrapper.layer.cornerRadius = 10
            headerCell.notiWrapper.clipsToBounds = true
            headerCell.notiWrapper.layer.shadowColor = UIColor.black.cgColor
            headerCell.notiWrapper.layer.shadowOffset = CGSize(width: 3, height: 3)
            headerCell.notiWrapper.layer.shadowRadius = 10.0
            headerCell.notiWrapper.layer.shadowOpacity = 0.2
            headerCell.notiWrapper.layer.masksToBounds = false
            return headerCell
        } else {
            cell.notiWrapper.layer.cornerRadius = 10
            cell.notiWrapper.clipsToBounds = true
            cell.notiWrapper.layer.shadowColor = UIColor.black.cgColor
            cell.notiWrapper.layer.shadowOffset = CGSize(width: 3, height: 3)
            cell.notiWrapper.layer.shadowRadius = 10.0
            cell.notiWrapper.layer.shadowOpacity = 0.2
            cell.notiWrapper.layer.masksToBounds = false
            return cell
        }
    }
}
