//
//  NotiTableViewCell.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/03/27.
//

import UIKit

class NotiTableViewCell: UITableViewCell {

    @IBOutlet weak var badgeView: UIView!
    @IBOutlet weak var notiTitle: UILabel!
    @IBOutlet weak var notiAt: UILabel!
    @IBOutlet weak var notiWrapper: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        badgeView.layer.cornerRadius = 4
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
