//
//  MyPageViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/04/02.
//

import UIKit

class MyPageViewController: UIViewController {
    
    @IBOutlet weak var backBtn: PressedView!
    @IBOutlet weak var profileImageAdd: PressedView!
    @IBOutlet weak var addedImage: UIImageView!
    @IBOutlet weak var pushSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let backGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleClose(sender:)))
        backBtn.addGestureRecognizer(backGesture)
        profileImageAdd.layer.cornerRadius = 10
        // 이미지 추가 했을시 false 처리
        addedImage.isHidden = true
        pushSwitch.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
    }
    
    @objc func handleClose(sender: ValueTapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func handleBack(sender: ValueTapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
    }
    
}
