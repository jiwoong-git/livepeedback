//
//  UserViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/04/02.
//

import UIKit

class UserViewController: UIViewController {

    @IBOutlet weak var emptyImageView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var inquiryBtn: PressedView!
    @IBOutlet weak var settingBtn: PressedView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.backgroundColor = UIColor(named: "BG_WHITE")
        navigationController?.navigationBar.isTranslucent = false

        let attrs = [
            NSAttributedString.Key.foregroundColor: UIColor(named: "BRIGHT_ORANGE_TWO"),
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .black)
        ]
        navigationController?.navigationBar.titleTextAttributes = attrs as [NSAttributedString.Key : Any]
        
        emptyImageView.layer.cornerRadius = 10
        // 프로필 이미지 있을때 뷰 처리
        profileImage.layer.cornerRadius = 10
        profileImage.isHidden = true
        inquiryBtn.layer.cornerRadius = 8
        inquiryBtn.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        let settingGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleSetting(sender:)))
        settingBtn.addGestureRecognizer(settingGesture)
    }
        
    @objc func handleSetting(sender: ValueTapGestureRecognizer) {
        let detailVC = MyPageViewController(nibName: "MyPageViewController", bundle: nil) as MyPageViewController
        
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
    
    @IBAction func searchBtn(_ sender: Any) {
        let searchVC = SearchViewController(nibName: "SearchViewController", bundle: nil) as SearchViewController
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    @IBAction func notificationBtn(_ sender: Any) {
        let notiVC = NotificationViewController(nibName: "NotificationViewController", bundle: nil) as NotificationViewController
        self.navigationController?.pushViewController(notiVC, animated: true)
    }
    
}
