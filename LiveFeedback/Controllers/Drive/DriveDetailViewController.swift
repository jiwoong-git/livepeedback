//
//  DriveDetailViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/06/26.
//

import UIKit

class DriveDetailViewController: UIViewController {

    @IBOutlet weak var backBtn: PressedView!
    @IBOutlet weak var deleteBtn: PressedView!
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var insertAt: UILabel!
    @IBOutlet weak var tagView: UIView!
    @IBOutlet weak var tagLabel: UILabel!
    @IBOutlet weak var previewImage: UIImageView!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var replyWrapper: UIView!
    @IBOutlet weak var replyText: UITextField!
    @IBOutlet weak var submitBtn: CustomUIButton!
    @IBOutlet weak var replyTextFieldTop: NSLayoutConstraint!
    @IBOutlet weak var replyTextFieldHeight: NSLayoutConstraint!
    
    @IBOutlet weak var trainerReplyWrapper: UIView!
    @IBOutlet weak var trainerImage: UIImageView!
    @IBOutlet weak var trainerName: UILabel!
    @IBOutlet weak var trainerReplyInsertAt: UILabel!
    @IBOutlet weak var trainerReplyContent: UILabel!
    @IBOutlet weak var trainerReplyTop: NSLayoutConstraint!
    @IBOutlet weak var trainerReplyHeight: NSLayoutConstraint!
    
    
    var replyVisible: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitView()
        let backGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleClose(sender:)))
        backBtn.addGestureRecognizer(backGesture)
        checkReplyView()
    }
    
    @objc func handleClose(sender: ValueTapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }

    private func setInitView() {
        userProfileImage.layer.cornerRadius = 10
        tagView.layer.cornerRadius = 13
        previewImage.layer.cornerRadius = 10
        trainerImage.layer.cornerRadius = 10
    }
    
    private func checkReplyView() {
        if replyVisible {
            replyTextFieldHeight.constant = 0
            replyWrapper.isHidden = true
            replyTextFieldTop.isActive = false
            
            trainerReplyWrapper.isHidden = false
            trainerReplyTop.isActive = true
        } else {
            trainerReplyWrapper.isHidden = true
            trainerReplyHeight.constant = 0
            trainerReplyTop.isActive = false
            
            replyWrapper.isHidden = false
            replyTextFieldTop.isActive = true
        }
    }

}
