//
//  UploadViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/06/24.
//

import UIKit

class UploadViewController: UIViewController {

    @IBOutlet weak var backBtn: PressedView!
    @IBOutlet weak var previewImage: UIImageView!
    @IBOutlet weak var arrowDownImage: UIImageView!
    @IBOutlet weak var selectView: UIView!
    @IBOutlet weak var exerciseBtn: CustomUIButton!
    @IBOutlet weak var dietBtn: CustomUIButton!
    @IBOutlet weak var btnWapper: UIView!
    @IBOutlet weak var uploadBtn: CustomUIButton!
    @IBOutlet weak var contentTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitView()
        contentTextView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    func setInitView() {
        let backGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleBack(sender:)))
        backBtn.addGestureRecognizer(backGesture)
        previewImage.layer.cornerRadius = 10
        arrowDownImage.transform = CGAffineTransform(rotationAngle: CGFloat(-(Double.pi / 2)))
        arrowDownImage.image = arrowDownImage.image?.withRenderingMode(.alwaysTemplate)
        arrowDownImage.tintColor = UIColor(named: "DRIVE_BOTTOM_LINE")
        selectView.layer.cornerRadius = 10
        selectView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        dietBtn.layer.cornerRadius = 10
        dietBtn.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        btnWapper.layer.cornerRadius = 10
        btnWapper.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        uploadBtn.layer.cornerRadius = 10
        contentTextView.layer.cornerRadius = 10
        contentTextView.textContainerInset = UIEdgeInsets(top: 18, left: 18, bottom: 18, right: 18);
        textViewSetupView()
    }
    
    @objc func handleBack(sender:ValueTapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // UITextView PlaceHolder 설정
    func textViewSetupView() {
        if contentTextView.text == "이 사진에 대해서 설명을 남겨주세요." {
            contentTextView.text = ""
            contentTextView.textColor = UIColor.black
           
        } else if contentTextView.text == "" {
            contentTextView.text = "이 사진에 대해서 설명을 남겨주세요."
            contentTextView.textColor = UIColor(named: "UPLOAD_PLACE_HOLDER")
        }
   }
}

extension UploadViewController: UITextViewDelegate {
    // 편집이 시작될때
    func textViewDidBeginEditing(_ textView: UITextView) {
        textViewSetupView()
    }
    
    // 편집이 종료될때
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textViewSetupView()
        }
    }
    
    // 텍스트가 입력될때
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        // 개행시 최초 응답자 제거
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
}
