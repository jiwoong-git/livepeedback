//
//  DriveViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/04/03.
//

import UIKit

class DriveViewController: UIViewController {
    
//    @IBOutlet weak var titleLabelView: UIView!
    @IBOutlet weak var toggleBtn: CustomUIButton!
    @IBOutlet weak var uploadCollectionView: UICollectionView!
    @IBOutlet weak var topButtonWrapper: UIView!
    @IBOutlet weak var imageBtn: PressedView!
    @IBOutlet weak var videoBtn: PressedView!
    @IBOutlet weak var uploadBtnView: UIView!
    
    @IBOutlet weak var uploadBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var cameraBtn: CustomUIButton!
    @IBOutlet weak var albumBtn: CustomUIButton!
    @IBOutlet weak var fileBtn: CustomUIButton!
    
    var heightConstraint: NSLayoutConstraint!
    
    var toggled: Bool = false
    
    var bottomToggled: Bool = false
    
    var commonAlertModalVc: CommonAlertModalViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setInitView()
        
        uploadCollectionView.delegate = self
        uploadCollectionView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        toggled = false
        bottomToggled = false
        heightConstraint.isActive = true
        topButtonWrapper.layer.zPosition = 0
        uploadBtnView.isHidden = true
        uploadBtnHeight.constant = 0
    }
    
    func setInitView() {
        heightConstraint = topButtonWrapper.heightAnchor.constraint(equalToConstant: 0)
        heightConstraint.isActive = true
        topButtonWrapper.layer.cornerRadius = 8
        topButtonWrapper.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        videoBtn.layer.cornerRadius = 8
        videoBtn.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        cameraBtn.layer.cornerRadius = 8
        cameraBtn.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        uploadBtnHeight.constant = 0
        uploadBtnView.isHidden = true
        cameraBtn.setTitleColor(UIColor(named: "BG_WHITE"), for: .highlighted)
        albumBtn.setTitleColor(UIColor(named: "BG_WHITE"), for: .highlighted)
        fileBtn.setTitleColor(UIColor(named: "BG_WHITE"), for: .highlighted)
        
    }
    
    @IBAction func searchBtn(_ sender: Any) {
        let searchVC = SearchViewController(nibName: "SearchViewController", bundle: nil) as SearchViewController
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    @IBAction func notificationBtn(_ sender: Any) {
        let notiVC = NotificationViewController(nibName: "NotificationViewController", bundle: nil) as NotificationViewController
        self.navigationController?.pushViewController(notiVC, animated: true)
    }

    @IBAction func handleToggle(_ sender: Any) {
        toggled = !toggled
        if toggled {
            heightConstraint.isActive = false
            self.topButtonWrapper.layer.zPosition = 5
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            }, completion: {finished in
            })
        } else {
            heightConstraint.isActive = true
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            }, completion: {finished in
                self.topButtonWrapper.layer.zPosition = 0
            })
        }
    }
   
    
    
    
    @objc func handleDirvePlus(sender: ValueTapGestureRecognizer) {
        bottomToggled = !bottomToggled
        checkBottomToggle()
    }
    
    func checkBottomToggle() {
        if bottomToggled {
            uploadBtnView.isHidden = false
            uploadBtnHeight.constant = 98
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            }, completion: {finished in
            })
        } else {
            uploadBtnView.isHidden = true
            uploadBtnHeight.constant = 0
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            }, completion: {finished in
            })
        }
    }
    
    @IBAction func handleCamera(_ sender: CustomUIButton) {
        
        if let uploadVc = UploadViewController(nibName: "UploadViewController", bundle: nil) as? UploadViewController {
            if let navigator = navigationController {
                uploadVc.hidesBottomBarWhenPushed = true
                navigator.navigationBar.isHidden = true
                navigator.pushViewController(uploadVc, animated: true)
            }
        }
    }
    @IBAction func handleAlbum(_ sender: Any) {
        if let uploadVc = UploadViewController(nibName: "UploadViewController", bundle: nil) as? UploadViewController {
            if let navigator = navigationController {
                uploadVc.hidesBottomBarWhenPushed = true
                navigator.navigationBar.isHidden = true
                navigator.pushViewController(uploadVc, animated: true)
            }
        }
    }
    @IBAction func handleFile(_ sender: Any) {
        commonAlertModalVc = CommonAlertModalViewController(nibName: "CommonAlertModalViewController", bundle: nil) as? CommonAlertModalViewController
        if commonAlertModalVc != nil {
            commonAlertModalVc!.definesPresentationContext = true
            commonAlertModalVc!.modalPresentationStyle = .overCurrentContext
            commonAlertModalVc!.message = "권한 요청을 허용해주세요."
            self.present(commonAlertModalVc!, animated: true) {
                self.commonAlertModalVc!.alertBtn.addTarget(self, action: #selector(self.handleCloseModal), for: .touchUpInside)
            }
        }
    }
    
    @objc func handleCloseModal() {
        if commonAlertModalVc != nil {
            commonAlertModalVc!.dismiss(animated: true) {
                self.commonAlertModalVc = nil
            }
        }
    }
}

extension DriveViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6 + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let lastRowIndex = collectionView.numberOfItems(inSection: 0)
        if indexPath.row == (lastRowIndex - 1) {
            
            guard let drivePlusCell = collectionView.dequeueReusableCell(withReuseIdentifier: "drivePlusCell", for: indexPath) as? DrivePlusCollectionViewCell else {
                return UICollectionViewCell()
            }
            
            let drivePlusGesture = ValueTapGestureRecognizer(target: self, action: #selector(handleDirvePlus(sender:)))
            drivePlusCell.addGestureRecognizer(drivePlusGesture)
            return drivePlusCell
        } else {
            if true {
                guard let driveVideoCell = collectionView.dequeueReusableCell(withReuseIdentifier: "driveVideoCell", for: indexPath) as? DriveVideoCollectionViewCell else {
                    return UICollectionViewCell()
                }
                return driveVideoCell
            } else {
                guard let driveImageCell = collectionView.dequeueReusableCell(withReuseIdentifier: "driveImageCell", for: indexPath) as? DriveImageCollectionViewCell else {
                    return UICollectionViewCell()
                }
                return driveImageCell
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if let driveDetailVc = DriveDetailViewController(nibName: "DriveDetailViewController", bundle: nil) as? DriveDetailViewController {
            if let navigator = navigationController {
                driveDetailVc.hidesBottomBarWhenPushed = true
                navigator.navigationBar.isHidden = true
                navigator.pushViewController(driveDetailVc, animated: true)
            }
        }
    }
}

extension DriveViewController: UICollectionViewDelegateFlowLayout {
    // 사이즈
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      let collectionViewCellWithd = collectionView.frame.width / 3 - 4
        
        return CGSize(width: collectionViewCellWithd, height: collectionViewCellWithd)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }

    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
}
