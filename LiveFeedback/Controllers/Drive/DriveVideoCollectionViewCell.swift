//
//  DriveVideoCollectionViewCell.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/04/03.
//

import UIKit

class DriveVideoCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var thumImg: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var playTimeView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        playTimeView.layer.cornerRadius = 15 / 2
        
    }

}
