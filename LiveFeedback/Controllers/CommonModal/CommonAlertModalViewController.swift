//
//  CommonAlertModalViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/06/28.
//

import UIKit

class CommonAlertModalViewController: UIViewController {

    @IBOutlet weak var alertModal: UIView!
    @IBOutlet weak var alertText: UILabel!
    @IBOutlet weak var alertBtn: UIButton!
    
    var message:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(named: "BG_BLACK")?.withAlphaComponent(0.75)
        view.isOpaque = false
        alertModal.layer.cornerRadius = 10
        alertBtn.layer.cornerRadius = 10
        alertText.text = message ?? "안내"
    }

}
