//
//  CommonConfirmModalViewController.swift
//  LiveFeedback
//
//  Created by 박지웅 on 2021/06/28.
//

import UIKit

class CommonConfirmModalViewController: UIViewController {
    
    @IBOutlet weak var alertModal: UIView!
    @IBOutlet weak var alertText: UILabel!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var confirmBtn: UIButton!
    
    var message:String?
    var confirmText: String? = "확인"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(named: "BG_BLACK")?.withAlphaComponent(0.75)
        view.isOpaque = false
        alertModal.layer.cornerRadius = 10
        cancelBtn.layer.cornerRadius = 10
        confirmBtn.layer.cornerRadius = 10
        alertText.text = message ?? "안내"
        confirmBtn.setTitle(confirmText, for: .normal)
    }
    
    @IBAction func handleCancel(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
}
