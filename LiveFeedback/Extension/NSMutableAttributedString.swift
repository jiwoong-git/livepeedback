//
//  UILabelExtension.swift
//  ureca
//
//  Created by minizz on 2021/01/21.
//

import Foundation
import UIKit

extension NSMutableAttributedString {
    
    func bold(text: String, fontSize: CGFloat) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont.boldSystemFont(ofSize: fontSize)]
        self.append(NSMutableAttributedString(string: text, attributes: attrs))
        
        return self
    }
    
    func normal(text: String, fontSize: CGFloat) -> NSMutableAttributedString {
        let attrs: [NSAttributedString.Key: Any] = [.font: UIFont.systemFont(ofSize: fontSize)]
        self.append(NSMutableAttributedString(string: text, attributes: attrs))
        
        return self
    }
    
    func attributedText(boldText: String, normalText: String, fontSize: CGFloat) -> NSMutableAttributedString {
        return NSMutableAttributedString()
            .bold(text: boldText, fontSize: fontSize)
            .normal(text: normalText, fontSize: fontSize)
    }
}
