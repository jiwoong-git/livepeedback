//
//  CustomUIButton.swift
//

import Foundation
import UIKit

class CustomUIButton: UIButton {
    
    private var bgColor: UIColor? = UIColor(named: "PRIMARY090")
    private var tintBgColor: UIColor? = UIColor(named: "PRIMARY050")
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        bgColor = self.backgroundColor
        tintBgColor = self.tintColor
    }
    
    override var isHighlighted: Bool {
        didSet {
            if (isHighlighted) {
                self.backgroundColor = tintBgColor
            }
            else {
                self.backgroundColor = bgColor
            }
        }
    }
    
    func setBgColor(color: UIColor) {
        bgColor = color
        self.backgroundColor = bgColor
    }
    func setTintBgColor(color: UIColor) {
        tintBgColor = color
        self.tintColor = tintBgColor
    }
}
