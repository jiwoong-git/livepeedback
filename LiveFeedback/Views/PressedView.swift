import UIKit

class PressedView: UIView {
    
    private var bgColor: UIColor? = UIColor.white
    private var pressedColor: UIColor? = UIColor.white
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        bgColor = self.backgroundColor
        pressedColor = self.tintColor
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        backgroundColor = pressedColor
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        backgroundColor = bgColor
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        backgroundColor = bgColor
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        if let touch = touches.first as UITouch? {
            let currentPoint = touch.location(in: self)
            if (!self.point(inside: currentPoint, with: event)) {
                touchesCancelled(touches, with: event)
            }
        }
    }
    
    func setBackgroundColor(color: UIColor) {
        bgColor = color
        self.backgroundColor = bgColor
    }
    func setTintColor(color: UIColor) {
        pressedColor = color
        self.tintColor = pressedColor
    }
}
